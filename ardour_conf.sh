#!/bin/bash
echo "Install Ardour"
sudo add-apt-repository ppa:dobey/audiotools
sudo apt-get update
sudo apt-get install ardour
echo "add limits for audio"
(sudo echo @audio          -       rtprio          99; sudo echo @audio          -       memlock		   unlimited; sudo echo @audio          -       nice            -19;) >> /etc/security/limits.conf
echo "add user to group audio"
sudo addgroup $USER audio
echo "install Jack"
sudo apt-get install jackd qjackctl
#restart pulse audio after using jack
#pulseaudio --kill
#pulseaudio --start
